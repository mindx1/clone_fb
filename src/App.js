import { useState } from "react";
import CallApi from "./utils/callApi.util";

function App() {
  const [input, setInput] = useState({
    lastName: "",
    firstName: "",
    email: "",
    password: "",
  });

  const onchangeInput = (e) => {
    setInput((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  const onClickSignup = async () => {
    const result = await CallApi("auth/signup", "POST", input);
    console.log(result);
  };

  return (
    <div>
      <input onChange={onchangeInput} name="lastName" placeholder="last name" />
      <input
        onChange={onchangeInput}
        name="firstName"
        placeholder="first name"
      />
      <input onChange={onchangeInput} name="email" placeholder="email" />
      <input onChange={onchangeInput} name="password" placeholder="password" />
      <button onClick={() => onClickSignup()}>Dang Ky</button>
    </div>
  );
}

export default App;
