export default async function APICaller(
  endpoint,
  method = "GET",
  data,
  params
) {
  const result = await fetch(
    `http://localhost:4001/${endpoint}${params ? "?" + params : ""}`,
    {
      method: method,
      body: data ? JSON.stringify(data) : null,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      },
    }
  )
    .then(async (response) => {
      return await response.json();
    })
    .catch((e) => console.log(e));
  return result;
}
